import heapq

grades = [110, 25, 38, 49,
        20, 95, 33, 87, 90]

print(min(grades), max(grades))

print(heapq.nlargest(3, grades))

print(heapq.nsmallest(4, grades))